from tasks.models import Task
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect, render
from tasks.forms import TaskCreateForm
from django.contrib.auth.decorators import login_required

# Create your views here.


# @login_required()
# def task_create_view(request):
#     if request.method == "POST":
#         form = TaskCreateForm(request.POST)
#         if form.is_valid():
#             create = form.save(commit=False)
#             create.assignee = request.user
#             create.save()
#             return redirect("show_project", create.project_id)
#     else:
#         form = TaskCreateForm()
#     context = {"form": form}
#     return render(request, "tasks/create.html", context)


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    context_object_name = "create_task"

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.project_id])

  
class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    context_object_name = "show_my_tasks"
    template_name = "tasks/list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ["is_completed"]
    context_object_name = "complete_task"
    success_url = reverse_lazy("show_my_tasks")
